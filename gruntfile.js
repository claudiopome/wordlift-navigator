module.exports = function(grunt) {
	
	// Configure task(s)
	grunt.initConfig({
		
		pkg: grunt.file.readJSON('package.json'),
		
		compass: {                 
   			build: {                   
      			options: {              
        			sassDir: 'scss',
        			cssDir: 'css',
       				benvironment: 'production'
      			}
    		},
    	
    		dev: {                    
      			options: {
      				config: 'config.rb',
        			sassDir: 'scss',
        			cssDir: 'css'
      			}
    		}
  		},

  		uglify: {
			build: {
				src: 'js/*.js',
				dest: 'js/script.min.js'	
			},
			dev: {
				options: {
					beautify: true,
					mangle: false,
					compress: false,
					preserveComments: 'all'
				},
				src: 'src/js/*.js',
				dest: 'js/script.min.js'
			}
		},

		watch: {
			js: {
				files: ['js/*.js'],
				tasks: ['uglify:dev']
			},

			css: {
				files: ['scss/**/*.scss'],
				tasks: ['compass:dev']
			},
			livereload: {
      			options: { livereload: true },
      			files: ['css/**/*'],
   			 }
		},
		
	});
	
	// Load the plugins
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');


	
	//Register task(s)
	grunt.registerTask('default', ['compass:dev', 'uglify:dev', ]);
	grunt.registerTask('build', ['compass:build', 'uglify:build']);
}